//
//  DetailViewController.h
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIWebViewDelegate> {
    
    IBOutlet UIWebView *webView;
    
}

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *imageString;
@property (strong, nonatomic) NSString *urlString;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

