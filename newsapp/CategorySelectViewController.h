//
//  CategorySelectViewController.h
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MasterViewController.h"

@interface CategorySelectViewController : UITableViewController {
    
    AppDelegate *appDelegate;
    
}

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (nonatomic,strong) MasterViewController *masterView;


- (IBAction) closeCategoryView: (id)sender;

@end
