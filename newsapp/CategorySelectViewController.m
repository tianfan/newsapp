//
//  CategorySelectViewController.m
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import "CategorySelectViewController.h"

@interface CategorySelectViewController ()

@end

@implementation CategorySelectViewController

@synthesize appDelegate;

@synthesize masterView;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}
*/

- (IBAction) closeCategoryView {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 appDelegate.limit = 20;
                                 appDelegate.offset = 0;
                                 self.masterView.newses = nil;
                                 [self.masterView.tableView reloadData];
                                 [self.masterView callNYTimesWebService];
                                 self.masterView.title = appDelegate.currentCategory;
                             }];
    
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.row)
    {
        case 0:
            appDelegate.currentCategory = @"All";
            break;
        case 1:
            appDelegate.currentCategory = @"Business";
            break;
        case 2:
            appDelegate.currentCategory = @"Technology";
            break;
        case 3:
            appDelegate.currentCategory = @"Science";
            break;
        case 4:
            appDelegate.currentCategory = @"Health";
            break;
        case 5:
            appDelegate.currentCategory = @"Sports";
            break;
        case 6:
            appDelegate.currentCategory = @"Arts";
            break;
        case 7:
            appDelegate.currentCategory = @"Style";
            break;
        case 8:
            appDelegate.currentCategory = @"Food";
            break;
        case 9:
            appDelegate.currentCategory = @"Real+Estate";
            break;
        case 10:
            appDelegate.currentCategory = @"Travel";
            break;
        default:
            appDelegate.currentCategory = @"All";
            break;
            
    }
    
    [self closeCategoryView];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
