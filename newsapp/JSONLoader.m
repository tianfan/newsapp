//
//  JSONLoader.m
//  newsapp
//
//  Created by jacktian on 2014-12-28.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import "JSONLoader.h"
#import "News.h"

@implementation JSONLoader

@synthesize appDelegate;

- (void)locationsFromJSONFile:(NSURL *)url {
    
    /*
    // Create a NSURLRequest with the given URL
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                         timeoutInterval:3.0];
    // Get the data
    NSURLResponse *response;
     
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    // Now create a NSDictionary from the JSON data
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    // Create a new array to hold the news
    NSMutableArray *newses = [[NSMutableArray alloc] init];
    
    // Get an array of dictionaries with the key "news"
    NSArray *array = [jsonDictionary objectForKey:@"results"];
    
    // Iterate through the array of dictionaries
    for(NSDictionary *dict in array) {
    
        // Create a new News object for each one and initialise it with information in the dictionary
        News *news= [[News alloc] initWithJSONDictionary:dict];
        
        // Add the News object to the array
        
        //NSLog(@"added a news: %@", news.title);
        
        if (![self checkObjectExist:news]) {
            [self insertNewObject:news];
            [newses addObject:news];
        }
        //[self insertNewObject:news];
    }
    
    */
    
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    
    // 1
    sessionConfig.allowsCellularAccess = YES;
    
    // 2
    //[sessionConfig setHTTPAdditionalHeaders:
     //@{@"Accept": @"application/json"}];
    
    // 3
    sessionConfig.timeoutIntervalForRequest = 10.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    sessionConfig.HTTPMaximumConnectionsPerHost = 100;
    sessionConfig.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    
    //NSURLSession *session = [NSURLSession sharedSession];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                          delegate:self
                                                     delegateQueue:nil];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        // Create a new array to hold the news
        NSMutableArray *newses = [[NSMutableArray alloc] init];
        
        // Get an array of dictionaries with the key "news"
        NSArray *array = [json objectForKey:@"results"];
        
        // Iterate through the array of dictionaries
        for(NSDictionary *dict in array) {
            
            // Create a new News object for each one and initialise it with information in the dictionary
            News *news= [[News alloc] initWithJSONDictionary:dict];
            
            // Add the News object to the array
            
            //NSLog(@"added a news: %@", news.title);
            
            if (![self checkObjectExist:news]) {
                [self insertNewObject:news];
                [newses addObject:news];
            }

        }
        
        //JackTianDelegationStuff
        [self.delegate didFininshRequestWithJson];
        
    }];
    
    [dataTask resume];
    
    
    // Return the array of Location objects
    //return newses;
}


- (BOOL) checkObjectExist: (News *) news {
    
    NSError * error;
    NSFetchRequest * checkExistance = [[NSFetchRequest alloc] init];
    [checkExistance setEntity:[NSEntityDescription entityForName:@"News" inManagedObjectContext:self.managedObjectContext]];
    [checkExistance setFetchLimit:1];
    [checkExistance setPredicate:[NSPredicate predicateWithFormat:@"title == %@", news.title]];
    NSManagedObject *theobject = [[self.managedObjectContext executeFetchRequest:checkExistance error:&error] lastObject];
    
    if (theobject) {
        
        return true;
        
    } else {
        
        return false;
        
    }
    
}


- (void)insertNewObject:(News *) news {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    //NSLog(@"saving new object: %@", news.title);
    
    NSManagedObjectContext *context = [self managedObjectContext];

    NSManagedObject *newManagedObject = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"News"
                                       inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    [newManagedObject setValue:news.title forKey:@"title"];
    [newManagedObject setValue:news.abstract forKey:@"abstract"];
    [newManagedObject setValue:news.url forKey:@"url"];
    [newManagedObject setValue:news.thumbnail_standard forKey:@"thumbnail_standard"];
    [newManagedObject setValue:news.source forKey:@"source"];
    [newManagedObject setValue:appDelegate.currentCategory forKey:@"category"];
    [newManagedObject setValue:news.byline forKey:@"byline"];
    
    NSString * time = news.created_date;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    NSDate *date = [df dateFromString:time];
    
    [newManagedObject setValue:date forKey:@"created_date"];
    
    NSString * time1 = news.published_date;
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    NSDate *date1 = [df1 dateFromString:time1];
    
    
    [newManagedObject setValue:date1 forKey:@"published_date"];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSData *data = [NSData dataWithContentsOfURL:location];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self.progressView setHidden:YES];
        //[self.imageView setImage:[UIImage imageWithData:data]];
    });
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    float progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self.progressView setProgress:progress];
    });
}

@end
