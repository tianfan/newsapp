//
//  AppDelegate.h
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *currentCategory;

@property int limit;
@property int offset;

// The choice of local persistance storage: CoreData with sqlite.
//Core Data is not perfect but it will mainly help in the auxiliary facets of the application - things like data persistence, presentation:

//Core Data manages save and undo functionality for you. It has a persistent store, which tracks changes, and can be flushed to the disk automatically at any number of times, perfect for news reader.
//Core Data and related classes provide easy ways to get your entities into UITableViews, like NSFetchedResultsController. I apparently didn't use this feature, it is great, but it has bugs to fit in my logic.
//Core Data abstracts away a lot of the messy things you'd otherwise have to deal with yourself, such as lists of objects, one-to-many or many-to-many relationships, or constraints on object attributes, into a single nice clean object-oriented interface.

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

