//
//  News.h
//  newsapp
//
//  Created by jacktian on 2014-12-28.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary;

@property (readonly) NSString *title;
@property (readonly) NSString *abstract;
@property (readonly) NSString *url;
@property (readonly) NSString *thumbnail_standard;
@property (readonly) NSString *source;
@property (readonly) NSString *category;
@property (readonly) NSString *byline;
@property (readonly) NSString *created_date;
@property (readonly) NSString *published_date;


@end
