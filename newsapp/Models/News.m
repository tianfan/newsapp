//
//  News.m
//  newsapp
//
//  Created by jacktian on 2014-12-28.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import "News.h"

@implementation News

// Init the object with information from a dictionary
- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary {
    if(self = [self init]) {
        // Assign all properties with keyed values from the dictionary
        _title = [jsonDictionary objectForKey:@"title"];
        _abstract = [jsonDictionary objectForKey:@"abstract"];
        _url = [jsonDictionary objectForKey:@"url"];
        _thumbnail_standard = [jsonDictionary objectForKey:@"thumbnail_standard"];
        _source = [jsonDictionary objectForKey:@"source"];
        _category = [jsonDictionary objectForKey:@"section"];
        _byline = [jsonDictionary objectForKey:@"byline"];
        _created_date = [jsonDictionary objectForKey:@"created_date"];
        _published_date = [jsonDictionary objectForKey:@"published_date"];

    }
    return self;
}


@end
