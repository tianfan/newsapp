//
//  SettingsViewController.h
//  newsapp
//
//  Created by jacktian on 2015-01-04.
//  Copyright (c) 2015 jacktian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController

- (IBAction) closeSettingsView: (id)sender;

- (IBAction) goToSettings: (id)sender;


@end
