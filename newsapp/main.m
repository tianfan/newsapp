//
//  main.m
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
