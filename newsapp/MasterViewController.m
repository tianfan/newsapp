//
//  MasterViewController.m
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "CategorySelectViewController.h"
#import "JSONLoader.h"
#import "News.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

@synthesize appDelegate;
@synthesize newses;
@synthesize managedObjectContext;
@synthesize initialLoading, loadmore, activityIndicator, refreshControl;

//@synthesize fetchedResultsController = _fetchedResultsController;

    //NSArray *_newses;

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}


/*- (void)viewWillAppear:(BOOL)animated {
    
    self.title = appDelegate.currentCategory;
    
    NSLog(@"current cat is: %@", appDelegate.currentCategory);
    
    appDelegate.offset = 0;
    loadmore = NO;
    
    /*
    if ([self checkIfNewsEmptyForThisCategory:appDelegate.currentCategory]) {
        
        appDelegate.limit = 20;
        appDelegate.offset = 0;
        initialLoading = YES;
        
        for (int i = 0; i < 2; i++) {
            [self callNYTimesWebService];
            appDelegate.offset = appDelegate.offset + 20;
            
            NSLog(@"current limit: %d", appDelegate.limit);
            NSLog(@"current offset: %d", appDelegate.offset);
        }
        
        appDelegate.offset = 0;
        appDelegate.limit = 100;
        initialLoading = NO;
        
        if (!initialLoading) {
            [self callNYTimesWebService];
        }
        
    } else {
        
        [self callNYTimesWebService];
        
    }
     */
//   [self callNYTimesWebService];
    
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.toolbarHidden = NO;
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].leftBarButtonItem = barButton;
    //self.loadingIcon = barButton;
    //[activityIndicator startAnimating];
    
    UIBarButtonItem *buttonItem;
    
    buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"Category" style:UIBarButtonItemStylePlain target:self action:@selector( loadCategoryView)];
    
    //loadingIcon = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    
    self.toolbarItems = [ NSArray arrayWithObjects:buttonItem, loadingIcon, nil ];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    initialLoading = NO;
    loadmore = NO;

    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    // Do any additional setup after loading the view, typically from a nib.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;

    /*UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
*/
    self.newses = [[NSArray alloc] init];
    
    self.title = appDelegate.currentCategory;
    
    [self callNYTimesWebService];

}

- (void)refresh:(UIRefreshControl *)refreshControl {
    appDelegate.limit = 20;
    appDelegate.offset = 0;

    [self callNYTimesWebService];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //NSLog(@"**************************************in here doing setting stuff...");

    
    if ([[segue identifier] isEqualToString:@"showDetail"] || [[segue identifier] isEqualToString:@"showDetail2"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        //NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        News *news = [newses objectAtIndex:indexPath.row];
        
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:news];
        controller.urlString = news.url;
        controller.imageString = news.thumbnail_standard;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
        

    }
    
    if([@"tocategory" isEqualToString:[segue identifier]]) {
        

            
        //CategorySelectViewController *destController = (CategorySelectViewController*)[segue
                                                                 //destinationViewController];
        
        CategorySelectViewController *destController = (CategorySelectViewController *)[[segue destinationViewController] topViewController];
        
        destController.masterView = self;
    }
}

 

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
    if (selectedRowIndex.row == [self.newses count])
    {
        
        return NO;
        
    } else {
    
        return YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.newses count] + 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"newses count is: %lu", (unsigned long)[self.newses count]);
    
    if([self.newses count] > 0 && indexPath.row < [self.newses count])
    {
    
        News *temp_object = [self.newses objectAtIndex:indexPath.row];
        
        // Get user preference
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL showpicture = [defaults boolForKey:@"enable_picture_setting"];
        
        
        if ([[[temp_object valueForKey:@"thumbnail_standard"] description] isEqualToString:@"" ] || !showpicture)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellNoImage" forIndexPath:indexPath];
            [self configureCell:cell atIndexPath:indexPath];
            return cell;
            
            
        } else {
            
            
            if (indexPath.row == [self.newses count]) {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadmore" forIndexPath:indexPath];
                [self configureCell:cell atIndexPath:indexPath];
                return cell;
                
                
            } else {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
                [self configureCell:cell atIndexPath:indexPath];
                return cell;
                
            }
            
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadmore" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==[self.newses count]){
        loadmore = YES;
        appDelegate.offset = appDelegate.offset + 20;
        [self callNYTimesWebService];
        //[self callNYTimesWebServiceWithoutLoadingDB];
    } /*else {
        
        News *news = [newses objectAtIndex:indexPath.row];
        
        //DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        
        DetailViewController* detailViewController = [[DetailViewController alloc] init];
        
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        [detailViewController setDetailItem:news];
        detailViewController.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        detailViewController.navigationItem.leftItemsSupplementBackButton = YES;
        
    }*/
}

/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
            
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row==[self.newses count]){
        
        UILabel *title = (UILabel *)[cell viewWithTag:2];
        title.text = [NSString stringWithFormat:@"%@", @"load more"];
        
    } else {
    
        NSManagedObject *object = [self.newses objectAtIndex:indexPath.row];
        
        UILabel *title = (UILabel *)[cell viewWithTag:2];
        title.text = [NSString stringWithFormat:@"%@", [[object valueForKey:@"title"] description]];
        [title setFont:[UIFont fontWithName:@"Helvetica" size: [[[NSUserDefaults standardUserDefaults] valueForKey:@"font_size_setting"] integerValue]]];
        [title setNumberOfLines:0];
        [title sizeToFit];
        
        UILabel *abstract = (UILabel *)[cell viewWithTag:3];
        abstract.text = [NSString stringWithFormat:@"%@", [[object valueForKey:@"abstract"] description]];

        
        UILabel *category = (UILabel *)[cell viewWithTag:4];
        category.text = [NSString stringWithFormat:@"%@  %@", [[object valueForKey:@"category"] description], [[object valueForKey:@"created_date"] description]];
        
        
        UIImageView *imageViewThumb = (UIImageView *)[cell viewWithTag:1];
        
        imageViewThumb.image = [UIImage imageNamed:@"placeholderimage.png"];
        
        
        if ([[[object valueForKey:@"thumbnail_standard"] description] isEqualToString:@"" ])
        {
            
            imageViewThumb.image = [UIImage imageNamed:@"placeholderimage.png"];
            
        } else {
            
            NSURL *imageURL = [NSURL URLWithString:[[object valueForKey:@"thumbnail_standard"] description]];
            /*
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Update the UI
                    imageViewThumb.image = [UIImage imageWithData:imageData];
                });
            });
             */
            
            // download the image asynchronously
            [self downloadImageWithURL:imageURL completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded) {
                    // change the image in the cell
                    imageViewThumb.image = image;
                    
                }
            }];
        }
    
    
    }
    

}


- (void) loadDataFromDB {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"News" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:appDelegate.limit];
    [fetchRequest setFetchOffset:appDelegate.offset];
    
    //NSLog(@"current limit: %d", appDelegate.limit);
    //NSLog(@"current offset: %d", appDelegate.offset);
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_date" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category = %@", appDelegate.currentCategory];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    
    if (!loadmore) {
        self.newses = [managedObjectContext executeFetchRequest:fetchRequest error:&error];

    } else {
    
    
        NSArray *temp_newses = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        //[self.newses addObjectsFromArray:[managedObjectContext executeFetchRequest:fetchRequest error:&error]];
        
        NSArray *combined = [self.newses arrayByAddingObjectsFromArray:temp_newses];
        self.newses = combined;
        
        loadmore = NO;
    }

}

#pragma mark JackTianJsonLoader Delegate Methods
-(void)didFininshRequestWithJson
{
    //Process the responseJson;
    [self loadDataFromDB];
    
    //NSLog(@"******** going to reload the tableview***************");
    
    //[self.tableView reloadData];
    //[self stopActivityIndicator];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    [self performSelectorOnMainThread:@selector(stopActivityIndicator) withObject:nil waitUntilDone:YES];
}

- (void) callNYTimesWebService {
    
    // Create a new JSONLoader with a local file URL
    JSONLoader *jsonLoader = [[JSONLoader alloc] init];
    
    jsonLoader.delegate = self;
    
    jsonLoader.managedObjectContext = self.managedObjectContext;
    
    NSString *url_string = [NSString stringWithFormat:@"http://api.nytimes.com/svc/news/v3/content/all/%@/.json?limit=%d&offset=%d&api-key=20eee6344a99455dd26538357e1001c5:4:70586218", appDelegate.currentCategory, appDelegate.limit, appDelegate.offset];
    
    //NSLog(@"current_url_string: %@", url_string);
    
    NSURL *url = [NSURL URLWithString:url_string];
    
    [self.activityIndicator startAnimating];
    
    // Load the data on a background queue...
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [jsonLoader locationsFromJSONFile:url];
        

    
        //[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        //[self performSelectorOnMainThread:@selector(stopActivityIndicator) withObject:nil waitUntilDone:YES];
        

    });

    
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void) stopActivityIndicator {
    [self.activityIndicator stopAnimating];
    [refreshControl endRefreshing];

}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
    

}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    //NSLog(@"Done loading stuff..");
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


- (BOOL) checkIfNewsEmptyForThisCategory: (NSString *) category {
    
    NSError * error;
    NSFetchRequest * checkExistance = [[NSFetchRequest alloc] init];
    [checkExistance setEntity:[NSEntityDescription entityForName:@"News" inManagedObjectContext:self.managedObjectContext]];
    [checkExistance setFetchLimit:1];
    [checkExistance setPredicate:[NSPredicate predicateWithFormat:@"category == %@", category]];
    NSManagedObject *theobject = [[self.managedObjectContext executeFetchRequest:checkExistance error:&error] lastObject];
    
    if (theobject) {
        
        return false;
        
    } else {
        
        return true;
        
    }
    
}

- (IBAction)loadMoreAction {
    
    loadmore = YES;
    appDelegate.offset = appDelegate.offset + 20;
    [self callNYTimesWebService];
    
}

- (void) loadCategoryView {
    
    [self performSegueWithIdentifier:@"tocategory" sender:self];
    
}

@end
