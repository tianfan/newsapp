//
//  JSONLoader.h
//  newsapp
//
//  Created by jacktian on 2014-12-28.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@protocol JackTianJsonLoaderDelegate <NSObject>

@required
-(void)didFininshRequestWithJson;
@end


@interface JSONLoader : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>
{
    
    AppDelegate *appDelegate;
    
}

// Return an array of News objects from the json file at location given by url
- (void)locationsFromJSONFile:(NSURL *)url;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (nonatomic,strong) NSObject < JackTianJsonLoaderDelegate > *delegate;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
