//
//  DetailViewController.m
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import "DetailViewController.h"
#import <Social/Social.h>

@interface DetailViewController ()

@end

@implementation DetailViewController

UIView* loadingView;

@synthesize webView, activityIndicator;


#pragma mark - Managing the detail item
- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"abstract"] description];
        self.title = [[self.detailItem valueForKey:@"title"] description];
        
        
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
        [self navigationItem].rightBarButtonItem = barButton;
        [activityIndicator startAnimating];
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    webView.delegate = self;
    
    NSString *fullURL = [[self.detailItem valueForKey:@"url"] description];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareTapped:(id)sender {
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    [sharingItems addObject:self.title];

    if ([self.imageString isEqualToString:@"" ]) {
        [sharingItems addObject:[UIImage imageNamed:@"placeholderimage.png"]];
    } else {
        [sharingItems addObject:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imageString]]]];
        
    }

    [sharingItems addObject:self.urlString];

    
    /*UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    activityController.popoverPresentationController.sourceView = self.p;
    [self presentViewController:activityController animated:YES completion:nil];
    */
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else
    {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        NSLog(@"%f",self.view.frame.size.width/2);
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    

        
}


@end
