//
//  MasterViewController.h
//  newsapp
//
//  Created by jacktian on 2014-12-26.
//  Copyright (c) 2014 jacktian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "JSONLoader.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate, NSURLConnectionDelegate,JackTianJsonLoaderDelegate>
{
    
    NSMutableData *_responseData;
    
    AppDelegate *appDelegate;
    
    UIBarButtonItem *loadingIcon;
    
}
- (void) callNYTimesWebService;
- (IBAction) loadMoreAction;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void) loadCategoryView;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (nonatomic, strong) NSArray *newses;

@property (nonatomic, strong) UIBarButtonItem *loadingIcon;

@property BOOL initialLoading;
@property BOOL loadmore;




@end

